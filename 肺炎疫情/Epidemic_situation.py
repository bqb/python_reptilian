from json import loads
from requests import get
from pyecharts.charts import Bar
import time



url='https://view.inews.qq.com/g2/getOnsInfo?name=disease_h5'
content=loads(get(url).json()['data'])
times = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))

#当前数据
def Current_data(json,times):

    #当前时段的的数据
    lower_level = json['chinaTotal']
    print(times)
    print('-'*30)
    print('当前确诊:{}'.format(lower_level['confirm']))
    print('疑似病人:{}'.format(lower_level['suspect']))
    print('当前死亡人数:{}'.format(lower_level['dead']))
    print('当前治愈的人数:{}'.format(lower_level['heal']))


    #做出可视化页面
    bar=Bar()
    bar.add_xaxis(['当前确诊','疑似病人','当前死亡人数','当前治愈的人数'])
    bar.add_yaxis(times,[lower_level['confirm'],lower_level['suspect'],
                         lower_level['dead'],lower_level['heal']])

    print('-'*15)
    # 较昨日增加了多少
    lower_level2 = json['chinaAdd']
    print('增加了{}人确诊'.format(lower_level2['confirm']))
    print('增加了{}人疑似病人'.format(lower_level2['suspect']))
    print('增加了{}人死亡'.format(lower_level2['dead']))
    print('增加了{}人治愈'.format(lower_level2['heal']))
    print('-' * 15)
#所有省份数据可视化
def Province_data(content):
    #地名
    Place_name = []

    #确诊数据
    Diagnosis_data = []

    #疑似数据
    Suspected_data= []

    #死亡数据
    Dead_data = []

    #治愈数据
    Cure_data = []

    bar=Bar()
    for i in range(34):
        Introduction=content['areaTree'][0]['children'][i]
        #添加地名
        Place_name.append(Introduction['name'])
        #确诊
        Diagnosis_data.append(Introduction['total']['confirm'])
        #疑似
        Suspected_data.append(Introduction['total']['suspect'])
        #死亡
        Dead_data.append(Introduction['total']['dead'])
        #治愈
        Cure_data.append(Introduction['total']['heal'])
    bar.add_xaxis(Place_name)
    bar.add_yaxis('确诊',Diagnosis_data)
    bar.add_yaxis('疑似',Suspected_data)
    bar.add_yaxis('死亡',Dead_data)
    bar.add_yaxis('治愈',Cure_data)
    bar.render('各省数据.html')
    print('-' * 15)
    print('程序结束后 会生成你当前文件夹下')
    print('-' * 15)


#各个省份的确诊 死亡 治愈 疑似
def Province(content):

    for i in range(34):
        #print(content['areaTree'][0]['children'][0]['total'])
        Province_name = content['areaTree'][0]['children'][i]

        #当前数据
        Lookup = Province_name['total']
        # 新增数据
        Add_data=Province_name['today']
        print('-'*15,Province_name['name'],'的数据如下','-'*15)
        print('总计情况：   确诊{}     疑似{}    死亡{}    治愈{}  '.format(Lookup['confirm'],Lookup['suspect'],
                                                            Lookup['dead'],Lookup['heal']))
        print('今日新增:    确诊{}    疑似{}    死亡{}    治愈{} '.format(Add_data['confirm'],Add_data['suspect'],
                                                              Add_data['dead'],Add_data['heal']))
        print('-'*40)


#查询省份下
def Local_province(content):
    #地名
    Place_name = []

    #确诊数据
    Diagnosis_data = []

    #疑似数据
    Suspected_data= []

    #死亡数据
    Dead_data = []

    #治愈数据
    Cure_data = []
    content1 = content["areaTree"][0]["children"]
    Name_data={}
    count = 0
    for i in content1:
        # 地名
        name1 = i['name']
        # {'湖北': 0}{'浙江': 1}
        dict2 = {name1: count}
        Name_data.update(dict2)

        count = count + 1
    a=input('请输入你要查询的省份:')

    test=Name_data[a]
    Entering_the_province=content["areaTree"][0]["children"][test]



    #print(Entering_the_province)
    print('-'*10,'你查询的  %s省  数据如下'%a,'-'*10)
    print('省内总计数据   确诊:{}   疑似:{}   死亡:{}   治愈:{}'.format(Entering_the_province['total']['confirm'],Entering_the_province['total']['suspect'],
                                                      Entering_the_province['total']['dead'],Entering_the_province['total']['heal']))
    print('省内今日新增   确诊:{}   疑似:{}   死亡:{}   治愈:{}'.format(Entering_the_province['today']['confirm'],
                                                          Entering_the_province['today']['suspect'],
                                                          Entering_the_province['today']['dead'],
                                                          Entering_the_province['today']['heal']))
    print('-'*50)
    Enter_the_city = Entering_the_province['children']
    Length = len(Enter_the_city)


    for i in range(Length):
        #市名
        City=Enter_the_city[i]#['total']
        data=City['total']
        today_data=City['today']
        City_Name=City['name']
        print('-'*30)
        print('总数据      城市:{}    确诊:{}   疑似:{}   死亡:{}   治愈:{}'.format(
            City_Name,data['confirm'],data['suspect'],data['dead'],data['heal']
        ))
        print('今日数据     确诊:{}    疑似:{}    死亡:{}     治愈'.format(
            today_data['confirm'],today_data['suspect'],today_data['dead'],today_data['heal']
        ))

        print('-'*30)



        Place_name.append(City_Name)
        # 确诊
        Diagnosis_data.append(data['confirm'])
        # 疑似
        Suspected_data.append(data['suspect'])
        # 死亡
        Dead_data.append(data['dead'])
        # 治愈
        Cure_data.append(data['heal'])
    b=input('已经加载好可视化数据是否下载(1 需要 2不需要):')
    bar=Bar()
    if b == '1':
        bar.add_xaxis(Place_name)
        bar.add_yaxis('确诊', Diagnosis_data)
        bar.add_yaxis('疑似', Suspected_data)
        bar.add_yaxis('死亡', Dead_data)
        bar.add_yaxis('治愈', Cure_data)
        bar.render('{}省内数据.html'.format(a))
        print('-' * 15)
        print('程序结束后 会生成你当前文件夹下')
        print('-' * 15)

    else:
        print('好吧')

print('-'*30)
print('欢迎使用全国疫情查询系统1.0')
print('现实疫情期间请尽量减少外出 外出请带好口罩')
print('勤洗手  多消毒  ')
print('-'*30)
while   True:
    time.sleep(5)
    print('1.查询全国疫情数据\n2.各省份疫情数据可视化\n3.各省份疫情数据\n4.查询某省份数据\n其他键可退出')
    Eat=input('请输入对应的您要查询的模块:')

    if Eat == '1':
        Current_data(content,times)


    elif Eat == '2':
        Province_data(content)

    elif Eat == '3':
        Province(content)

    elif Eat == '4':
        Local_province(content)

    else:
        print('已退出')
        break







